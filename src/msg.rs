use serde::{Deserialize, Serialize};

#[derive(Deserialize, Copy, Clone, Eq, PartialEq)]
#[serde(rename_all = "kebab-case")]
pub enum OrbitalPermissions {
    /// Allows the extension to create widgets (popups, OSDs etc)
    OSD,

    /// Allows extensions to manage clients which aren't theirs
    ManageForeignClients,

    /// Allows extensions to fake inputs
    FakeInput,

    /// Allows extensions to capture screenshots of clients or the desktop
    CaptureBuffer,
}

pub struct PermBuilder {
    osd: bool,
    manage_foreign_clients: bool,
    fake_input: bool,
    capture_buffer: bool,
}

impl OrbitalPermissions {
    pub fn bitflags() -> PermBuilder {
        PermBuilder {
            osd: false,
            manage_foreign_clients: false,
            fake_input: false,
            capture_buffer: false,
        }
    }
}

impl PermBuilder {
    pub fn osd(self) -> Self {
        return Self {
            osd: true,
            ..self
        }
    }
    
    pub fn manage_foreign_clients(self) -> Self {
        return Self {
            manage_foreign_clients: true,
            ..self
        }
    }
    
    pub fn fake_input(self) -> Self {
        return Self {
            fake_input: true,
            ..self
        }
    }
    
    pub fn capture_buffer(self) -> Self {
        return Self {
            capture_buffer: true,
            ..self
        }
    }
    
    pub fn done(self) -> i32 {
        let mut bitflags = 0i32;
        
        if self.osd { bitflags &= 0b0001; }
        if self.manage_foreign_clients { bitflags &= 0b0010; }
        if self.fake_input { bitflags &= 0b0100; }
        if self.capture_buffer { bitflags &= 0b1000; }
        
        return bitflags;
    }
}

#[derive(Deserialize)]
pub enum Message {
    RequestData(RequestData),
    CreateOSD(CreateOSD),
    Geometry(Geometry),
}

/// Ask for a specific piece of information
#[derive(Deserialize)]
pub enum RequestData {
    /// Gets the buffer of a window
    WindowContent {
        window_id: usize,
        // TODO: Shmem parameters
    },
    /// Gets the buffer of the entire display
    DisplayContent {
        // TODO: Shmem parameters
    },

    WindowGeometry {
        window_id: usize,
    },
}

/// Ask to open an OSD
#[derive(Deserialize)]
pub struct CreateOSD {
    initial_geometry: Geometry,
}

#[derive(Serialize, Deserialize)]
pub struct Geometry {
    pub width: u64,
    pub height: u64,
    pub x: i64,
    pub y: i64,
    pub z: i64,
}

#[derive(Serialize)]
pub enum Signal {}