use log::error;

use log::info;
use orbital_extensions::Extension;
use orbital_extensions::Handler;
use orbital_extensions::OrbitalPermissions;

pub struct VolumeOsd;

impl Handler for VolumeOsd {
    fn new(_name: &str) -> Self {
        info!("spawning");
        return Self;
    }

    fn error<Err: std::error::Error>(&mut self, err: Err) {
        error!("{:?}", err);
    }
}

pub fn main() -> std::io::Result<()> {
    return Extension::<VolumeOsd>::new("volume-osd", OrbitalPermissions::bitflags().osd())?
        .run();
}
