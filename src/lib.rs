use std::{
    path::PathBuf,
    fs::OpenOptions,
    fs::File,
    io::Read,
    io::Result,
    error::Error,
    collections::VecDeque,
    os::unix::fs::OpenOptionsExt,
};

use serde_json::Deserializer;

pub mod msg;
pub use msg::*;

pub struct Extension<Handler: crate::Handler> {
    backing: File,
    handler: Handler,
    msg_queue: VecDeque<msg::Message>,
    sig_queue: VecDeque<msg::Signal>,
}

impl<Handler: crate::Handler> Extension<Handler> {
    pub fn new<Name: AsRef<str>>(name: Name, perm: PermBuilder) -> Result<Self> {
        Ok(Self {
            backing: OpenOptions::new()
                .read(true)
                .write(true)
                .custom_flags(perm.done())
                .open(PathBuf::from("/scheme/orbital-ext/")
                    .join(name.as_ref()))?,
            handler: Handler::new(name.as_ref()),
            msg_queue: VecDeque::new(),
            sig_queue: VecDeque::new()
        })
    }
    
    fn update(&mut self) -> Result<()> {
        Ok(())
    }
    
    /// Causes the extension host break out of the read call by issuing an `fsync` syscall. This informs the extension host that something has happened that needs attention.
    fn skip(&self) -> Result<()> {
        return self.backing.sync_all();
    }
    
    pub fn run(&mut self) -> Result<()> {
        let mut msg = String::new();

        loop {
            match self.backing.read_to_string(&mut msg) {
                Ok(0) => continue,
                Ok(len) => for msg in Deserializer::from_str(&msg[0..len]).into_iter() {
                    match msg {
                        Ok(msg) => self.msg_queue.push_back(msg),
                        Err(err) => self.handler.error(err)
                    }
                },
                Err(err) => self.handler.error(err)
            }
            
            if let Err(err) = self.update() {
                self.handler.error(err);
            }
            
            // TODO: send signals
        }
    }
}

pub trait Handler {
    fn new(name: &str) -> Self;
    
    fn error<Err: Error>(&mut self, _err: Err) {}
}